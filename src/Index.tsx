import * as React from 'react';
import {render} from 'react-dom';

import {Deck} from './deck/Deck';
import './main.scss';

render(<Deck />, document.getElementById('App'));
