import * as classNames from 'classnames';
import * as React from 'react';

import {Card, CardSuits} from '../card/Card';
import * as styles from './Deck.scss';

export interface DeckState {
  cards: JSX.Element[];
  cardsShown: JSX.Element[];
}

export class Deck extends React.PureComponent<{}, DeckState> {
  constructor(props: {}, state: DeckState) {
    super(props, state);

    this.state = {
      cards: [],
      cardsShown: [],
    };
  }

  componentWillMount() {
    const suits: string[] = Object.keys(CardSuits);
    suits.forEach((suit: string) => {
      for (let i = 1; i < 14; i++) {
        this.state.cards.push(<Card key={`${suit}-${i}`} value={i} suit={CardSuits[suit]} />);
      }
    });
  }

  render(): JSX.Element {
    const dealOneCardClass: string = classNames(
      styles.button,
      {
        [styles.buttonDisabled]: !this.state.cards.length,
      },
    );
    return(
      <div className={styles.deck}>
        {this.state.cards}
        {this.state.cardsShown}
        <div className={styles.buttons}>
          <button className={styles.button} onClick={() => this.shuffle()}>Shuffle Deck</button>
          <button className={dealOneCardClass} onClick={() => this.dealOneCard()} disabled={!this.state.cards.length}>Deal a Card</button>
          <button className={styles.button} onClick={() => this.resetDeck()}>Reset Deck</button>
        </div>
      </div>
    );
  }

  private dealOneCard() {
    if (this.state.cards.length) {
      const newCard: JSX.Element = this.state.cards[this.state.cards.length - 1];

      this.setState({
        cards: this.state.cards.slice(0, -1),
        cardsShown: [
          ...this.state.cardsShown,
          <Card {...newCard.props} key={newCard.key} showFace />,
        ],
      });
    }
  }

  private shuffle() {
    this.resetDeck(() => {
      const cards: JSX.Element[] = this.state.cards;

      let counter = cards.length;

      while (counter > 0) {
        const index = Math.floor(Math.random() * counter);

        counter--;

        const temp = cards[counter];
        cards[counter] = cards[index];
        cards[index] = temp;
      }

      this.setState({
        ...this.state,
        cards,
      });
    });
  }

  private resetDeck(callback?: () => void) {
    const cardsToPutBack: JSX.Element[] = [];

    this.state.cardsShown.forEach((card: JSX.Element) => {
      cardsToPutBack.push(<Card {...card.props} key={card.key} showFace={false} />);
    });

    this.setState({
      cards: this.state.cards.concat(cardsToPutBack.reverse()),
      cardsShown: [],
    }, callback);
  }
}
