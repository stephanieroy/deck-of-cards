import {mount, ReactWrapper, shallow} from 'enzyme';
import * as React from 'react';

import {CardSuits} from '../card/Card';
import {Deck, DeckState} from './Deck';
import * as styles from './Deck.scss';

describe('<Deck />', () => {
  it('should render without errors', () => {
    expect(() => shallow(<Deck />)).not.toThrow();
  });

  describe('<Deck />', () => {
    let deck: ReactWrapper<{}, DeckState>;
    let deckInstance: any;

    beforeEach(() => {
      deck = mount(<Deck />);
      deckInstance = deck.instance();
    });

    afterEach(() => {
      deck.unmount();
    });

    it('should add the 52 cards to the cards state on mount', () => {
      expect(deck.state().cards.length).toBe(52);
    });

    it('should display the 52 cards "stacked" by default', ()  => {
      expect(deck.find('Card').length).toBe(52);
    });

    it('should display three buttons', () => {
      expect(deck.find('button').length).toBe(3);
    });

    it('should call shuffle when clicking the first button', () => {
      const shuffleSpy: jasmine.Spy = spyOn(deckInstance, 'shuffle');

      deck.find('button').first().simulate('click');
      expect(shuffleSpy).toHaveBeenCalledTimes(1);

      deck.find('button').first().simulate('click');
      expect(shuffleSpy).toHaveBeenCalledTimes(2);
    });

    it('should call dealOneCard when clicking the second button', () => {
      const dealOneCardSpy: jasmine.Spy = spyOn(deckInstance, 'dealOneCard');

      deck.find('button').at(1).simulate('click');
      expect(dealOneCardSpy).toHaveBeenCalledTimes(1);

      deck.find('button').at(1).simulate('click');
      expect(dealOneCardSpy).toHaveBeenCalledTimes(2);
    });

    it('should call resetDeck when clicking the last button', () => {
      const resetDeckSpy: jasmine.Spy = spyOn(deckInstance, 'resetDeck');

      deck.find('button').last().simulate('click');
      expect(resetDeckSpy).toHaveBeenCalledTimes(1);

      deck.find('button').last().simulate('click');
      expect(resetDeckSpy).toHaveBeenCalledTimes(2);
    });

    it('should have the class buttonDisabled on the second button if there are no more cards to deal', () => {
      expect(deck.find(`.${styles.buttonDisabled}`).length).toBe(0);
      for (let i = 1; i < 53; i++) {
        deck.find('button').at(1).simulate('click');
      }
      expect(deck.find(`.${styles.buttonDisabled}`).length).toBe(1);
    });

    it('should remove the last card from the deck and show it when calling dealOneCard', () => {
      expect(deck.state().cardsShown.length).toBe(0);

      deckInstance.dealOneCard();
      expect(deck.state().cardsShown.length).toBe(1);
      expect(deck.state().cards.length).toBe(51);

      deckInstance.dealOneCard();
      expect(deck.state().cardsShown.length).toBe(2);
      expect(deck.state().cards.length).toBe(50);
    });

    it('should not change the length of the dealt card and the deck if there are no more cards to deal when calling dealOneCard', ()  => {
      for (let i = 1; i < 53; i++) {
        deckInstance.dealOneCard();
      }

      expect(deck.state().cardsShown.length).toBe(52);
      expect(deck.state().cards.length).toBe(0);

      deckInstance.dealOneCard();
      deckInstance.dealOneCard();
      deckInstance.dealOneCard();

      expect(deck.state().cardsShown.length).toBe(52);
      expect(deck.state().cards.length).toBe(0);
    });

    it('should have add the showFace prop to true on cards dealt', () => {
      deckInstance.dealOneCard();
      expect(deck.state().cardsShown[0].props.showFace).toBe(true);
    });

    it('should have the deck in order on mount', () => {
      expect(deck.state().cards[0].props.value).toBe(1);
      expect(deck.state().cards[0].props.suit).toBe(CardSuits.Hearts);
      expect(deck.state().cards[4].props.value).toBe(5);
    });

    it('should reset the deck in the same order when calling resetDeck', () => {
      deckInstance.dealOneCard();
      deckInstance.dealOneCard();

      deckInstance.resetDeck();

      expect(deck.state().cards[51].props.value).toBe(13);
      expect(deck.state().cards[51].props.suit).toBe(CardSuits.Spades);

      expect(deck.state().cards[50].props.value).toBe(12);
      expect(deck.state().cards[50].props.suit).toBe(CardSuits.Spades);
    });

    it('should not show any cards when calling resetDeck', () => {
      deckInstance.dealOneCard();
      deckInstance.dealOneCard();

      expect(deck.state().cardsShown.length).toBe(2);
      expect(deck.state().cards.length).toBe(50);

      deckInstance.resetDeck();

      expect(deck.state().cardsShown.length).toBe(0);
      expect(deck.state().cards.length).toBe(52);
    });

    it('should set the showFace prop to false on cards back in the deck when calling resetDeck', () => {
      deckInstance.dealOneCard();
      deckInstance.dealOneCard();

      deckInstance.resetDeck();

      expect(deck.state().cards[51].props.showFace).toBe(false);
    });

    it('should call resetDeck when calling shuffle', () => {
      const resetDeckSpy: jasmine.Spy = spyOn(deckInstance, 'resetDeck');

      deckInstance.shuffle();

      expect(resetDeckSpy).toHaveBeenCalledTimes(1);
    });

    it('should change the order of the cards when calling shuffle', () => {
      const cardKeys: string[] = deck.state().cards.map((card: JSX.Element) => card.key.toString());

      deckInstance.shuffle();

      expect(deck.state().cards.map((card: JSX.Element) => card.key.toString())).not.toEqual(cardKeys);
    });
  });
});
