import {mount, ReactWrapper, shallow} from 'enzyme';
import * as React from 'react';

import {Card, CardProps, CardSuits} from './Card';
import * as styles from './Card.scss';

describe('<Card />', () => {
  const basicProps: CardProps = {
    value: 4,
    suit: CardSuits.Diamonds,
    showFace: true,
  };

  it('should render without errors', () => {
    expect(() => shallow(<Card {...basicProps} />)).not.toThrow();
  });

  describe('<Card />', () => {
    let card: ReactWrapper<CardProps>;

    const mountComponent = (props: Partial<CardProps> = {}) => {
      if (card && card.length) {
        card.unmount();
      }
      card = mount(<Card {...basicProps} {...props} />);
    };

    beforeEach(() => {
      mountComponent();
    });

    afterEach(() => {
      card.unmount();
    });

    it('should get an value as a prop', () => {
      expect(card.props().value).toBe(basicProps.value);
    });

    it('should get its suit as a prop', () => {
      expect(card.props().suit).toBe(basicProps.suit);
    });

    it('should get whether it shows the face of the card or not as a prop', () => {
      expect(card.props().showFace).toBe(true);
    });

    it('should display the value twice', () => {
      const value: string = card.props().value.toString();

      expect(card.find(`.${styles.topValue}`).text()).toBe(value);
      expect(card.find(`.${styles.bottomValue}`).text()).toBe(value);
    });

    it('should not display any value if the face is not shown', () => {
      mountComponent({showFace: false});
      expect(card.find(`.${styles.topValue}`).length).toBe(0);
      expect(card.find(`.${styles.bottomValue}`).length).toBe(0);
    });

    it('should display an "A" if the value of the card is 1', () => {
      mountComponent({value: 1});
      expect(card.find(`.${styles.topValue}`).text()).toBe('A');
    });

    it('should display a "J" if the value of the card is 11', () => {
      mountComponent({value: 11});
      expect(card.find(`.${styles.topValue}`).text()).toBe('J');
    });

    it('should display a "Q" if the value of the card is 12', () => {
      mountComponent({value: 12});
      expect(card.find(`.${styles.topValue}`).text()).toBe('Q');
    });

    it('should display a "K" if the value of the card is 13', () => {
      mountComponent({value: 13});
      expect(card.find(`.${styles.topValue}`).text()).toBe('K');
    });

    it('should display a heart icon if the suit is hearts', () => {
      mountComponent({suit: CardSuits.Hearts});
      expect(card.find(`.${styles.suit}`).text()).toBe('♥');
    });

    it('should display a diamond icon if the suit is diamonds', () => {
      expect(card.find(`.${styles.suit}`).text()).toBe('♦');
    });

    it('should display a club icon if the suit is clubs', () => {
      mountComponent({suit: CardSuits.Clubs});
      expect(card.find(`.${styles.suit}`).text()).toBe('♣');
    });

    it('should display a spade icon if the suit is spades', () => {
      mountComponent({suit: CardSuits.Spades});
      expect(card.find(`.${styles.suit}`).text()).toBe('♠');
    });

    it('should display a star icon if the suit is incorrectly set', () => {
      mountComponent({suit: 'and tie'});
      expect(card.find(`.${styles.suit}`).text()).toBe('★');
    });

    it('should not display any suit if the face is not shown', () => {
      mountComponent({showFace: false});
      expect(card.find(`.${styles.suit}`).length).toBe(0);
    });

    it('should have the cardFace class if it is showing the face of the card', () => {
      expect(card.find(`.${styles.cardFace}`).length).toBe(1);
      expect(card.find(`.${styles.cardBack}`).length).toBe(0);
    });

    it('should have the cardBack class if it is not showing the face of the card', () => {
      mountComponent({showFace: false});
      expect(card.find(`.${styles.cardBack}`).length).toBe(1);
      expect(card.find(`.${styles.cardFace}`).length).toBe(0);
    });

    it('should have the class suitRed if its suit is hearts or diamonds', () => {
      expect(card.find(`.${styles.suitRed}`).length).toBe(1);

      mountComponent({suit: CardSuits.Hearts});
      expect(card.find(`.${styles.suitRed}`).length).toBe(1);

      mountComponent({suit: CardSuits.Clubs});
      expect(card.find(`.${styles.suitRed}`).length).toBe(0);

      mountComponent({suit: CardSuits.Spades});
      expect(card.find(`.${styles.suitRed}`).length).toBe(0);

      mountComponent({suit: 'any'});
      expect(card.find(`.${styles.suitRed}`).length).toBe(0);
    });
  });
});
