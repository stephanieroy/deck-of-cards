export const card: string;
export const cardBack: string;
export const cardFace: string;
export const topValue: string;
export const bottomValue: string;
export const suit: string;
export const suitRed: string;
