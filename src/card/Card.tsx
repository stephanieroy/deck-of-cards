import * as classNames from 'classnames';
import * as React from 'react';

import * as styles from './Card.scss';

export interface CardProps {
  value: number;
  suit: string;
  showFace?: boolean;
}

export const CardSuits: {[key: string]: string} = {
  Hearts: 'hearts',
  Diamonds: 'diamonds',
  Clubs: 'clubs',
  Spades: 'spades',
};

export class Card extends React.PureComponent<CardProps> {
  render(): JSX.Element {
    const cardClass: string = classNames(
      styles.card,
      {
        [styles.cardBack]: !this.props.showFace,
        [styles.cardFace]: this.props.showFace,
        [styles.suitRed]: [CardSuits.Hearts, CardSuits.Diamonds].indexOf(this.props.suit) !== -1,
      },
    );
    const value: string = this.formatValue();

    return this.props.showFace
      ? (
        <div className={cardClass}>
          <div className={styles.topValue}>{value}</div>
          <div className={styles.suit}>{this.getSuitIcon()}</div>
          <div className={styles.bottomValue}>{value}</div>
        </div>
      )
      : <div className={cardClass}></div>;
  }

  private formatValue(): string {
    switch (this.props.value) {
      case 1:
        return 'A';
      case 11:
        return 'J';
      case 12:
        return 'Q';
      case 13:
        return 'K';
      default:
        return this.props.value.toString();
    }
  }

  private getSuitIcon(): string {
    switch (this.props.suit) {
      case CardSuits.Hearts:
        return '♥';
      case CardSuits.Diamonds:
        return '♦';
      case CardSuits.Clubs:
        return '♣';
      case CardSuits.Spades:
        return '♠';
      default:
        return '★';
    }
  }
}
