const webpackConfig = require('./webpack.config.test.js');

module.exports = (config) => {
  config.set({
    frameworks: ['jasmine'],

    files: ['./karma.entry.ts'],

    preprocessors: {
      './karma.entry.ts': ['webpack'],
    },

    mime: {
      'text/x-typescript': ['ts', 'tsx'],
    },

    webpack: webpackConfig,

    webpackServer: {
      noInfo: true,
    },

    reporters: ['mocha', 'coverage'],

    coverageReporter: {
      dir: 'coverage',
      reporters: [
        { type: 'json', subdir: '.', file: 'coverage.json' },
      ],
    },

    port: 9876,
    colors: true,

    browsers: ['ChromeHeadless'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox'],
      },
    },

    autoWatch: true,
    singleRun: true,

    browserNoActivityTimeout: 30000,
  });
};