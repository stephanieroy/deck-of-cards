import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

describe('Deck of Cards', () => {
  document.body.innerHTML = '<div id="App"></div>';

  Enzyme.configure({adapter: new Adapter()});

  const testsContext = require.context('./src', true, /\.spec\.ts(x?)$/);
  testsContext.keys().forEach(testsContext);

  const coverageContext = require.context('./src', true, /^((?!\.d).)*.ts(x?)$/);
  coverageContext.keys()
    .filter((file) => file.indexOf('.spec.') === -1)
    .forEach(coverageContext);
});
