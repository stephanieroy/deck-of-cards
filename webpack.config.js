const path = require('path');

module.exports = {
  entry: './src/Index.tsx',
  mode: 'development',
  output: {
    path: path.join(__dirname, '/dist/assets'),
    publicPath: '/assets/',
    filename: 'bundle.js',
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.ts(x?)$/i,
        exclude: [/node_modules/],
        use: {
          loader: 'tslint-loader',
        },
      },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'typings-for-css-modules-loader',
            options: {
              modules: true,
              scss: true,
              namedExport: true,
              localIdentName: '[name]-[local]-[hash:base64]',
            },
          },
          {
            loader: 'postcss-loader',
          },
          {
            loader: 'sass-loader',
          },
        ],
      },
      {
        test: /\.png$/,
        loader: 'file-loader?mimetype=image/png',
      },
    ],
  },
  devServer: {
    contentBase: './dist',
    host: '0.0.0.0',
    disableHostCheck: true,
  },
};