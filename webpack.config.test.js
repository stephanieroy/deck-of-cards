const webpack = require('webpack');
const webpackConfig = require('./webpack.config');

module.exports = {
  ...webpackConfig,
  entry: './karma.entry.ts',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('test'),
    }),
  ],
  module: {
    rules: [
      ...webpackConfig.module.rules,
      {
        enforce: 'post',
        test: /src\/*\w*\/*\w*\.(?:(?!spec)(?!scss)).+$/i,
        exclude: /(node_modules)/,
        loader: 'istanbul-instrumenter-loader',
      },
    ],
  },
  externals: {
    cheerio: 'window',
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
  devServer: undefined,
  output: undefined,
};